# Exemplo de como utilizar gradle e semantic release plugin

## 📋 Requisitos
- [Eclipse](https://company-learn.readme.io/docs/ambiente-de-desenvolvimento-company)
- [Java 1.8](http://downloads.company.com.br/downloads?app=JAVA&c=1)
- [Maven 3.8](https://maven.apache.org/install.html)
- [Gradle](https://company-learn.readme.io/docs/manual-para-uso-do-gradle-no-ambiente-company)
- [Lombok 1.18.24](https://projectlombok.org/download)
  
## 🔧 Instalação
1. Clonar este projeto
```bash
git clone https://git.company.com.br/module.git
```
2. Importar projeto no eclipse

----
# Semantic Release
Este projeto está utilizando o [semantic-release](https://semantic-release.gitbook.io/semantic-release/) onde utilizamos as mensagens de confirmação para determinar o impacto do consumidor das alterações na base de código. Seguindo convenções formalizadas para mensagens de commit, o semantic-release determina automaticamente o próximo número de versão semântica, gera um changelog e publica o lançamento, por convenção a mensagem deve ser escrita com um espaço entre o dois pontos e a descrição e sem ponto final.

| 🚀 | Mensagem de commit                                             |
| -------------- |--------------------------------------------------- |     
|feat |(São adições de novas funcionalidades e implantações ao código)|
|fix |(Essencialmente definem o tratamento de correções de bugs)|
|perf |(Uma alteração de código que melhora o desempenho)|

## Exemplos de mensagem:
~~~bash  
  git commit -m "feat: Geração boleto rápido"
~~~ 

~~~bash  
  git commit -m "fix: Ajuste calculo icms"
~~~ 

~~~bash  
  git commit -m "perf: Melhorando carga tela" 
~~~ 
